make -C libft/ fclean && make -C libft/
gcc -g -Wall -Wextra -Werror -I . -o get_next_line.o -c get_next_line.c
gcc -g -Wall -Wextra -Werror -I . -o get_next_line_main.o -c get_next_line_main.c
gcc -g -o test_gnl get_next_line_main.o get_next_line.o -I . -L libft/ -lft
