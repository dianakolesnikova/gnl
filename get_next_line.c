/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:21:25 by gmance-r          #+#    #+#             */
/*   Updated: 2019/02/15 19:53:51 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

void				ft_lstnew_fd(t_list **tail, size_t fd)
{
	t_list			*new_elem;

	if (!*tail)
	{
		if (!(*tail = (t_list *)malloc(sizeof(t_list))))
			return ;
		(*tail)->content = NULL;
		(*tail)->content_size = fd;
		(*tail)->next = NULL;
	}
	else
	{
		if (!(new_elem = (t_list *)malloc(sizeof(t_list))))
			return ;
		(*tail)->next = new_elem;
		new_elem->content = NULL;
		new_elem->content_size = fd;
		new_elem->next = NULL;
		(*tail) = (*tail)->next;
	}
}

static void			ft_getbeforen(char *temp, char **line, int end)
{
	char			*last_piece;
	size_t			i;
	char			*temp_line2;

	temp_line2 = NULL;
	i = 0;
	if (end < BUFF_SIZE)
	{
		while (temp[i] != '\n' && temp[i] != '\0')
			i++;
	}
	else
	{
		while (temp[i] != '\n')
			i++;
	}
	last_piece = ft_memalloc(i + 1);
	last_piece = ft_memcpy(last_piece, temp, i);
	last_piece[i] = '\0';
	(*line == NULL) ? (temp_line2 = ft_strdup(last_piece)) \
		: (temp_line2 = ft_strjoin(*line, last_piece));
	free(*line);
	*line = temp_line2;
	free(last_piece);
}

static int			ft_saveaftern(t_list **tail, char *temp_buf, \
					char **line, int end)
//static int			ft_saveaftern(t_list **tail, char *raw_line, \
//					char **line, int end)					
{
	char			*tail_line;
	char			*temp;

	// size_t          i;
    // char            *temp;
    // size_t          j;
    // char            *temp2;


	// if ((ft_strchr(temp_buf, '\n')))
	// {
	// 	if ((*tail)->content)
	// 		free((*tail)->content);
	// 	if (ft_strchr(temp_buf, '\n') + 1)
	// 		(*tail)->content = ft_strdup(ft_strchr(temp_buf, '\n') + 1);
	// //	printf("tail->content saveaftern = %s\n", (*tail)->content);
	// 	ft_getbeforen(temp_buf, line, end);
	// 	return (1);
	// }
	// else
	// {
	// 	if ((*tail)->content)
	// 	{	
	// 		free((*tail)->content);
	// 		(*tail)->content = NULL;
	// 	}
	// 	ft_getbeforen(temp_buf, line, end);
	// 	return (0);
	// }
	
	temp = ft_strdup(temp_buf);
	if (!(tail_line = ft_strchr(temp_buf, '\n')))
	//	tail_line = ft_strchr(temp_buf, '\0');
		tail_line = NULL;
	// if ((*tail)->content)
	// 	free((*tail)->content);
	if (tail_line && tail_line + 1)
	{
	//	free((*tail)->content);
		(*tail)->content = ft_strdup(tail_line + 1);
		ft_getbeforen(temp, line, end);
	}
	else
	{
		ft_getbeforen(temp, line, end);
		free((*tail)->content);
		(*tail)->content = NULL;
	}
	free(temp);
	//printf("tail->content saveaftern2 = %s\n", (*tail)->content);
	return (1);
	
}

static int			ft_copy_line(t_list **tail, char **line, int fd, char *temp_line3)
{
	char			buf[BUFF_SIZE + 1];
	int				end;
	char			*temp_line;
	char			*full_content;
	
	end = BUFF_SIZE;
	temp_line = NULL;
	if ((*tail)->content)
	{
		full_content = ft_strdup((*tail)->content);
		if (ft_strchr(full_content, '\n'))
			return (ft_saveaftern(tail, full_content, line, end));
		//Сначала записать контент в line, а уже потом отделять от \n *line = (*tail)->content
		
		/* *line = (*tail)->content;
		if (ft_strchr(*line, '\n'))
			return (ft_saveaftern(tail, *line, line, end)); */
		// if there is full line in content, copy the line
		if (ft_strchr(((*tail)->content), '\n'))
			return (ft_saveaftern(tail, (*tail)->content, line, end)); 
		
		// 
		// if (*(char*)((*tail)->content))
		// {
		// 	(*line == NULL) ? (temp_line = ft_strdup((*tail)->content)) \
		// 			: (temp_line = ft_strjoin(*line, (*tail)->content));
		// 	free(*line);
		// 	*line = temp_line;
		// }
		// free((*tail)->content);
		// (*tail)->content = NULL;
	
	}
	ft_bzero(buf, BUFF_SIZE + 1);
	while ((end = read(fd, buf, BUFF_SIZE)))
	{
		buf[end] = '\0';
		if ((ft_strchr(buf, '\n')) || end < BUFF_SIZE)
			return (ft_saveaftern(tail, buf, line, end));
		(*line == NULL) ? (temp_line3 = ft_strdup(buf)) \
					: (temp_line3 = ft_strjoin(*line, buf));
		free(*line);
		*line = temp_line3;
	}
	if (!((*tail)->content) && !(*line) && (!(end = read(fd, buf, BUFF_SIZE))))
		return (0);
	return (1);
}

int					get_next_line(const int fd, char **line)
{
	t_list			*tail;
	static t_list	*begin;
	char			*temp_line;
	int				res;

	if (fd < 0 || !line || BUFF_SIZE < 1 || read(fd, NULL, 0))
		return (-1);
	temp_line = NULL;
	*line = NULL;
	if (!begin)
		ft_lstnew_fd(&begin, (size_t)fd);
	tail = begin;
	while (!(tail->content_size == (size_t)fd || tail->next == NULL))
		tail = tail->next;
	if (tail->content_size != (size_t)fd)
		ft_lstnew_fd(&tail, (size_t)fd);
	//printf("tail->content gnl before %s\n", (tail)->content);
	//return (ft_copy_line(&tail, line, fd, temp_line));
	res = ft_copy_line(&tail, line, fd, temp_line);
	//printf("tail->content gnl %s\n", (tail)->content);
	return (res);
}
