/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:21:35 by gmance-r          #+#    #+#             */
/*   Updated: 2019/02/15 19:28:42 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 50

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <sys/stat.h> 
# include <fcntl.h>
# include "libft/libft.h"

int					get_next_line(const int fd, char **line);

#endif
