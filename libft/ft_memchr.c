/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 21:28:14 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/09 14:15:23 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memchr(const void *arr, int c, size_t n)
{
	unsigned char	*arr_uns;
	unsigned char	c_uns;

	arr_uns = (unsigned char *)arr;
	c_uns = (unsigned char)c;
	while (n)
	{
		if (*arr_uns == c_uns)
			return (arr_uns);
		arr_uns++;
		n--;
	}
	return (NULL);
}
