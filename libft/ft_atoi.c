/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 20:52:55 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/29 21:42:01 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_long_fail(long res, long buff, int sign)
{
	if (res - buff < buff && sign > 0)
		return (-1);
	return (0);
}

int			ft_atoi(const char *s)
{
	long	res;
	int		sign;
	long	buff;

	res = 0;
	sign = 1;
	while (*s == '\r' || *s == '\t' || *s == '\n' ||
			*s == '\v' || *s == '\f' || *s == ' ')
		s++;
	if (*s == '+' || *s == '-')
	{
		if (*s == '-')
			sign = -1;
		s++;
	}
	while (*s >= '0' && *s <= '9')
	{
		buff = res;
		res = res * 10 + *s - 48;
		if (res - buff < buff)
			return (ft_long_fail(res, buff, sign));
		s++;
	}
	return ((int)res * sign);
}
