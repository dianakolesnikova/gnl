/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 15:36:33 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/09 18:55:54 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	char	*s_ret;
	size_t	len;

	s_ret = (char *)s;
	len = ft_strlen(s);
	while (len > 0)
	{
		if (s[len] == (char)c)
			return (&s_ret[len]);
		len--;
	}
	if (s[len] == (char)c)
		return (&s_ret[len]);
	return (NULL);
}
