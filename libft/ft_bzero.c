/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 18:39:04 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/28 21:22:26 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				ft_bzero(void *s, size_t n)
{
	unsigned char	*s_unsd;
	size_t			i;

	s_unsd = (unsigned char *)s;
	i = 0;
	while (i < n)
	{
		s_unsd[i] = '\0';
		i++;
	}
}
