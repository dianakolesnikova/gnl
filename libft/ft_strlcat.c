/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 16:21:57 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 15:09:56 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t n)
{
	size_t	i;
	size_t	len_src;
	size_t	len_dst;
	size_t	res;

	i = 0;
	len_dst = ft_strlen(dst);
	len_src = ft_strlen(src);
	if (n <= len_dst)
		res = n + len_src;
	else
		res = len_dst + len_src;
	while ((i + len_dst) + 1 < n && src[i] != '\0')
	{
		dst[i + len_dst] = src[i];
		i++;
	}
	dst[i + len_dst] = '\0';
	return (res);
}
