/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/31 21:11:38 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:42:16 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		ft_countnbr(int n)
{
	int			i;

	i = 0;
	if (n < 0)
	{
		if (n == -2147483648)
			return (11);
		n = n * (-1);
		i++;
	}
	while (n > 9)
	{
		n = n / 10;
		i++;
	}
	i++;
	return (i);
}

static char		*ft_savenbr_pos(char *str, int n, int i)
{
	while (i >= 0)
	{
		str[i--] = n % 10 + 48;
		n = n / 10;
	}
	return (&str[0]);
}

static char		*ft_savenbr_neg(char *str, int n, int i)
{
	while (i > 0)
	{
		str[i--] = (n % 10) * (-1) + 48;
		n = n / 10;
	}
	str[0] = '-';
	return (&str[0]);
}

char			*ft_itoa(int n)
{
	char		*str;
	int			i;

	i = ft_countnbr(n);
	if (!(str = (char *)malloc(sizeof(char) * (i + 1))))
		return (NULL);
	str[i] = '\0';
	i--;
	if (n < 0)
		return (ft_savenbr_neg(str, n, i));
	else
		return (ft_savenbr_pos(str, n, i));
}
