/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/30 19:26:23 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:41:41 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	ft_strtrim_begin(char const *s)
{
	size_t		i;

	i = 0;
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	return (i);
}

static char		*ft_strtrim_copy(char const *s, char *s_trm,\
size_t start, size_t len)
{
	size_t		i;

	i = 0;
	while (i < len - 1)
	{
		s_trm[i] = s[i + start];
		i++;
	}
	s_trm[i] = '\0';
	return (s_trm);
}

static size_t	ft_len(size_t start, size_t end, size_t i)
{
	size_t		len;

	len = i - start - (end - 1);
	if (i - start <= 1)
		len = 1;
	return (len);
}

char			*ft_strtrim(char const *s)
{
	size_t		i;
	size_t		start;
	size_t		end;
	size_t		len;
	char		*s_trm;

	end = 0;
	i = 0;
	if (s)
	{
		start = ft_strtrim_begin(s);
		while (s[i++])
		{
			end++;
			if (!(s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i] != '\0')
				end = 0;
		}
		len = ft_len(start, end, i);
		if (!(s_trm = (char *)malloc(sizeof(char) * (len))))
			return (NULL);
		s_trm = ft_strtrim_copy(s, s_trm, start, len);
		return (s_trm);
	}
	return (NULL);
}
