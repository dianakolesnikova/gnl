/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 18:05:08 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:39:55 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strdup(const char *s)
{
	size_t	i;
	size_t	len;
	char	*sdup;

	i = 0;
	len = ft_strlen(s);
	if (!(sdup = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (i <= len)
	{
		sdup[i] = s[i];
		i++;
	}
	sdup[i] = '\0';
	return (sdup);
}
