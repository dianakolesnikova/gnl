/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 21:52:49 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/30 14:34:55 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isdigit(int c)
{
	unsigned char c_uns;

	if (c >= 0 && c <= 255)
	{
		c_uns = (unsigned char)c;
		if (c_uns >= '0' && c_uns <= '9')
			return (1);
	}
	return (0);
}
