/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 19:46:12 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/11 20:40:58 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *s1, const char *s2, size_t len)
{
	size_t	i;
	size_t	flag;
	size_t	len_s2;
	char	*s;

	i = 0;
	flag = 0;
	s = (char *)s1;
	len_s2 = ft_strlen(s2);
	if (len_s2 == 0)
		return (s);
	while (i < len && s[i])
	{
		while (s[i + flag] == s2[flag] && s[i + flag] && (i + flag < len))
		{
			flag++;
			if (flag == len_s2)
				return (&s[i]);
		}
		flag = 0;
		i++;
	}
	return (NULL);
}
