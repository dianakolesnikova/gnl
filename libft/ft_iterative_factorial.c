/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/12 17:37:52 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/12 17:50:01 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_iterative_factorial(int nb)
{
	int	factorial;

	if (nb > 12 || nb < 0)
		return (0);
	else
	{
		if (nb == 0)
			factorial = 1;
		else
		{
			factorial = nb;
			while (nb-- > 1)
				factorial = factorial * nb;
		}
		return (factorial);
	}
}
