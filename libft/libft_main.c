/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_main.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 18:41:08 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/18 11:18:14 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

/* 34. FT_STRMAPI PART 2 
char 	ft_test2(unsigned int i, char c)
{
	i++;
	if (c == 'a')
		return ('b');
	else 
		return ('0');
}
*/

/* 33. FT_STRMAP PART 2 
char 	ft_test(char c)
{
	if (c == 'a')
		return ('b');
	else 
		return ('0');
}
*/

/* FT_LSTMAP PART 2 
t_list *lstmap_test_fn (t_list *list)
{
	t_list *list2;

	list2 = malloc(sizeof(t_list));
	bzero(list2, sizeof(t_list));
	list2->content = malloc(list->content_size * 2);
	list2->content_size = list->content_size * 2;
	list2->content = "cdefgh";
	return (list2);
} */

/* FT_LSTMAP PART 3 
t_list *		lstmap_f(t_list *m) {
	t_list *	r = ft_lstnew("OK !", 5);
	(void)m;
	return (r);
}
*/

int main(void)
{

/* FT_LSTMAP 
	t_list	*list;
	t_list	*map;

	bzero((list = malloc(sizeof(t_list))), sizeof(t_list));
	bzero((list->next = malloc(sizeof(t_list))), sizeof(t_list));
	list->content_size = 4;
	list->content = strdup("abc");
	list->next->content_size = 4;
	list->next->content = strdup("abc");
	map = ft_lstmap(list, lstmap_test_fn);
	printf("%s\n", (map->next->content)); */

/*
	t_list *l = ft_lstnew(strdup(" 1 2 3 "), 8);
	t_list *ret;

	l->next = ft_lstnew(strdup("ss"), 3);
	l->next->next = ft_lstnew(strdup("-_-"), 4);
	ret = ft_lstmap(l, lstmap_f);
	printf("%s\n", ret->content);
	printf("%s\n", ret->next->content);
	printf("%s\n", ret->next->next->content);
*/

/* FT_LSTNEW 
	void const *content;
	char *str = "hello, i'm a data";
	size_t 	content_size = 18;
	size_t	i;
	t_list	*new_list;

	i = 0;
	
	content = &str[0];
	new_list = ft_lstnew(content, content_size);
	printf("%s\n", (new_list->content));
*/

/* 49. FT_PUTNBR_FD 
	int n = 55;
	int fd;

	fd = open("test.txt", O_RDWR);
	ft_putnbr_fd(n, fd);
*/

/* 48. FT_PUTENDL_FD */

/* 47. FT_PUTSTR_FD 
	int	fd;
	char *s = "abcd";

	fd = open("test.txt", O_RDWR);
	ft_putstr_fd(s, fd);
*/

/* 46. FT_PUTCHAR_FD 
	int	fd;
	char c = 'a';

	fd = open("test.txt", O_RDWR);
	ft_putchar_fd(c, fd);
*/

/* 45. FT_PUTNBR 
	int n = -2147483648;

	ft_putnbr(n);
*/

/* 44. FT_PUTENDL 
	char const *s = "abcdef";

	ft_putendl(s);
*/

/* 43. FT_PUTSTR 
	char const *s = "abcdef";

	ft_putstr(s);
*/

/* 42. FT_PUTCHAR 
	char const *c = "a";

	ft_putchar(c);
*/

/* 41. FT_ITOA 
	int	n = 2147483647;
	printf("%s\n", ft_itoa(n)); 
*/

/* 40. FT_STRSPLIT 
	char const *s = "abc abcd";
	char **s_new;

	s_new = ft_strsplit(s, ' ');
	// printf("%s\n", s_new);
	 printf("%s\n", s_new[0]);
	 printf("%s\n", s_new[1]);
	// printf("%s\n", s_new[3]);
	// printf("%s\n", s_new[4]);
	// printf("%s\n", s_new[5]); 
*/

/* 39a. FT_STRTRIM_EXTRA 
	char const *s = "aaafadaa";

	printf("%s\n", ft_strtrim_extra(s, 'a'));
*/

/* 39. FT_STRTRIM 
	char const *s = "  \t \t \n   \n\n\n\t";

	printf("%s\n", ft_strtrim(s));
*/

/* 38. FT_STRJOIN
	char const *s1 = "abcdfefghij";
	char const *s2 = "123";

	printf("%s\n", ft_strjoin(s1, s2));
*/
/* 37. FT_STRSUB 
	char const *s1 = "abcdfefghij";
	unsigned int start = 3;
	size_t	len = 6;

	printf("%s\n", ft_strsub(s1, start, len));
*/

/* 36. FT_STRNEQU 
	char const *s1 = "abcdf";
	char const *s2 = "abcdf";
	size_t	n = 6;

	printf("%d\n", ft_strnequ(s1, s2, n));
*/

/* 35. FT_STREQU 
	char const *s1 = "abcd";
	char const *s2 = "ABCDD";

	printf("%d\n", ft_strequ(s1, s2));
*/

/* 34. FT_STRMAPI PART 1 

	char const *s = "abcde";

	printf("%s\n", ft_strmapi(s, ft_test2));

*/

/* 33. FT_STRMAP PART 1 

	char const *s = "abcde";

	printf("%s\n", ft_strmap(s, ft_test));
*/

/* 32. FT_STRITERI */

/* 31. FT_STRITER */

/* 30. FT_STRCLR */

/* 29. FT_STRDEL */

/* 28. FT_STRNEW */

/* 27. FT_MEMALLOC */

/* 26. FT_TOLOWER 
	int c;

	c = 75;
	printf("%d\n", tolower(c));
	printf("%d\n", ft_tolower(c));
*/

/* 25. FT_TOUPPER 
	int c;

	c = 110;
	printf("%d\n", toupper(c));
	printf("%d\n", ft_toupper(c));
*/

/* 24. FT_ISPRINT 
	int c;

	c = 30;
	printf("%d\n", isprint(c));
	printf("%d\n", ft_isprint(c));
*/

/* 23. FT_ISASCII 
	int c;

	c = -127;
	printf("%d\n", isascii(c));
	printf("%d\n", ft_isascii(c));
*/

/* 22. FT_ISALNUM 
	int c;

	c = 5555555;
	printf("%d\n", isalnum(c));
	printf("%d\n", ft_isalnum(c));
*/

/* 21. FT_ISALPHA 
	int c;

	c = -100;
	printf("%d\n", isdigit(c));
	printf("%d\n", ft_isdigit(c));
*/

/* 20. FT_ISALPHA 
	int c;

	c = 97;
	printf("%d\n", isalpha(c));
	printf("%d\n", ft_isalpha(c));
*/

 /* at19. FT_ATOI 
	const char *s = "22222222222222222222";
	const char *s_ft = "22222222222222222222";

	printf("%d\n", ft_atoi(s));
	printf("%d\n", ft_atoi(s_ft));
*/

/* 18. FT_STRNCMP 
	const char 	*s1 = "Foo Ba Barz";
	const char	*s2 = "Foo";
	const char 	*s1_ft = "Foo Ba Barz";
	const char	*s2_ft = "Foo";
	size_t n = 3;

	printf("origin %d\n", strncmp(s1, s2, n));
	printf("mine %d\n", ft_strncmp(s1_ft, s2_ft, n));
*/

/* 17. FT_STRCMP 
	const char 	*s1 = "\0";
	const char	*s2 = "\200";
	const char 	*s1_ft = "\0";
	const char	*s2_ft = "\200";

	printf("origin %d\n", strcmp(s1, s2));
	printf("mine %d\n", ft_strcmp(s1_ft, s2_ft));
*/

/* 16. FT_STRNSTR 
	const char 	*s1 = "ozarabozaraboze123";
	const char	*s2 = "ozaraboze";
	const char 	*s1_ft = "ozarabozaraboze123";
	const char	*s2_ft = "ozaraboze";
	size_t len = 15;

	printf("%s\n", strnstr(s1, s2, len));
	printf("%s\n", ft_strnstr(s1_ft, s2_ft, len));
*/

/* 15. FT_STRSTR 
	const char 	*s1 = "Foo Ba Barz";
	const char	*s2 = "Bar";
	const char 	*s1_ft = "Foo Ba Barz";
	const char	q*s2_ft = "Bar";

	printf("%s\n", strstr(s1, s2));
	printf("%s\n", ft_strstr(s1_ft, s2_ft));
*/

/* 14. FT_STRRCHR 
	const char 	*s = "\0";
	const char	*s_ft = "\0";
	int 		c;

	c = 97;
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s_ft, c));
*/

/* 14. FT_STRCHR 
	const char 	*s;
	const char	*s_ft;
	int 		c;

	c = 5;
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s_ft, c));
*/

/* 13. FT_STRLCAT 1 
	char dst[0xF00] = "there is no stars in the sky";
	char dst_ft[0xF00] = "there is no stars in the sky";
	const char *src = "the cake is a lie !\0I'm hidden lol\r\n";
	const char *src_ft = "the cake is a lie !\0I'm hidden lol\r\n";
	size_t 	n = strlen("the cake is a lie !\0I'm hidden lol\r\n") + strlen("there is no stars in the sky");

	printf("original res %lu\n", strlcat(dst, src, n));
	printf("original dst %s\n", dst);
	printf("mine res %lu\n", ft_strlcat(dst_ft, src_ft, n));
	printf("mine dst %s\n", dst_ft);
*/

/* 13. FT_STRLCAT 2 
	char dst[10] = "a";
	char dst_ft[10] = "a";
	const char *src = "lorem ipsum dolor sit amet";
	const char *src_ft = "lorem ipsum dolor sit amet";
	size_t 	n = 0;

//	printf("original res %lu\n", strlcat(dst, src, n));
	printf("original dst %s\n", dst);
	printf("mine res %lu\n", ft_strlcat(dst_ft, src_ft, n));
	printf("mine dst %s\n", dst_ft);
*/

/* 12a. FT_STRNCAT 
	char dst[10] = "abcd\0";
	char dst_ft[10] = "abcd\0";
	const char *src = "12346\0";
	const char *src_ft = "12346\0";
	size_t n;

	n = 3;
	printf("original %s\n", strncat(dst, src, n));
	printf("mine %s\n", ft_strncat(dst_ft, src_ft, n));
*/

/* 12. FT_STRCAT 
	char dst[5] = "abcd\0";
	char dst_ft[5] = "abcd\0";
	const char *src = "1234\0";
	const char *src_ft = "1234\0";

	printf("original %s\n", strcat(dst_ft, src_ft));
	printf("mine %s\n", ft_strcat(dst_ft, src_ft));
*/

/* 11. FT_STRNCPY 

	char dst[30];
	char dst_ft[30];
	const char *src = "--> nyancat <--\n\r";
	const char *src_ft = "--> nyancat <--\n\r";
	size_t	n = 12;

	printf("original %s\n", strncpy(dst, src, n));
	printf("mine %s\n", ft_strncpy(dst_ft, src_ft, n));
*/

/* 10. FT_STRCPY 
	char dst[30];
	char dst_ft[30];
	const char *src;
	const char *src_ft;

//	printf("original %s\n", strcpy(dst, src));
	printf("mine %s\n", ft_strcpy(dst, src));
*/

/* 9. FT_STRDUP 
	const char *s;
	const char *s_ft;

	printf("original %s\n", strdup(s));
	printf("mine %s\n", ft_strdup(s_ft));
*/

/* 8. FT_STRLEN 
	const char *s = "";
	const char *s_ft = "";

	printf("original %lu\n", strlen(s));
	printf("mine %lu\n", ft_strlen(s_ft));
*/

/* 7. FT_MEMCMP 
	const void 	*s1;
	const void 	*s2;
	size_t 		n;
	char		*str1 = NULL;
	char		*str2 = NULL;
	int			res;

	const void 	*s1_ft;
	const void 	*s2_ft;
	char		*str1_ft = NULL;
	char		*str2_ft = NULL;
	n = 0;
	s1 = &str1[0];
	s2 = &str2[0];
	s1_ft = &str1_ft[0];
	s2_ft = &str2_ft[0];

	res = memcmp(s1, s2, n);
	printf("%d\n", res);

	res = ft_memcmp(s1_ft, s2_ft, n);
	printf("%d\n", res);
*/

/* 6. FT_MEMCHR 
const void 	*arr;
const void	*arr_ft;
int 		c;
size_t 		n;
char		str[8] = "abcdefg";
char		str_ft[8] = "abcdefg";
const void	*x;
const void	*y;

c = 48;
n = 4;
arr = &str[0];
arr_ft = &str_ft[0];
x = memchr(arr, c, n);
printf("%s\n", (unsigned char *)x);
y = ft_memchr(arr_ft, c, n);
printf("%s\n", (unsigned char *)y);
*/

/* 5. FT_MEMMOVE
	void *dst;
	void *ft_dst;
	const void *src;
	const void *ft_src;
	char *str_dst = NULL;
	char *str_src = NULL;
	char *ft_str_dst = NULL;
	char *ft_str_src = NULL;
	size_t n;
	size_t i;
	void *x;
	void *y;

	i = 0;
	n = 4;
	src = &str_src[0];
	dst = &str_dst[0];
	x = memmove(&str_dst[3], &str_dst[1], n);
	printf("%s\n", (char *)x); 

 	ft_src = &ft_str_src[0];
	ft_dst = &ft_str_dst[0];
	y =	ft_memmove(&ft_str_dst[3], &ft_str_dst[1], n);
	printf("%s\n", (char *)y); 
*/

/* 4. FT_MEMCCPY 
	void *dst;
	void *ft_dst;
	const void *src;
	const void *ft_src;
	char str_dst[5] = "abcd";
	char str_src[5] = "1234";
	char ft_str_dst[5] = "abcd";
	char ft_str_src[5] = "1234";
	int c;
	size_t n;
	size_t i;
	void *x;
	void *y;

	i = 0;
	n = 3;
	c = 51;
*/
/*
	src = &str_src[0];
	dst = &str_dst[0];
	x = memccpy(dst, src, c, n);
	printf("%s\n", (char *)x);
*/
/* 	while (str_dst[i] != '\0')
   	{
    	printf("%c\n", str_dst[i]);
    	i++;
  	}
*/
/*
	i = 0;
	ft_src = &ft_str_src[0];
	ft_dst = &ft_str_dst[0];
	y =	ft_memccpy(ft_dst, ft_src, c, n);
	printf("%s\n", (char *)y);
*/
/*	while (i < n)
    {
        printf("%c\n", ft_str_dst[i]);
        i++;
    }
*/

/* 3. FT_MEMCPY */
	void *dst;
	void *ft_dst;
	const void *src;
	const void *ft_src;
	size_t n;
	char str_dst[20] = "abcdefg123";
	//char *str_src;
	char ft_str_dst[20] = "abcdefg123";
	//char ft_str_src[] = ;
	size_t	i;

	i = 0;
	n = 5;
	
	//src = &str_src[0];
	
	dst = &str_dst[0];
	memcpy(dst + 3, dst, n);
	while (i < n)
    {
        printf("%c\n", str_dst[i]);
        i++;
    }
	printf("\n");
	i = 0;
	//ft_src = &ft_str_src[0];
	ft_dst = &ft_str_dst[0];
	ft_memcpy(ft_dst + 3, ft_dst, n);
	while (i < n)
    {
        printf("%c\n", ft_str_dst[i]);
        i++;
    }


/* 2. FT_BZERO 
	size_t  n;
    void    *s;
	void	*s_test;
    char    str[5];
	char	str_test[5];
    size_t  i;

    s = &str[0];
	s_test = &str_test[0];
    n = 3;
    i = 0;
    printf("n = %d\n", n); 
	ft_bzero(s, n);
    while (i < n)
    {
        printf("%c\n", str[i]);
        i++;
    }
	i = 0;
    printf("Original bzero\n");
    bzero(str_test, n);
    while (i < n)
    {
        printf("%c\n", str_test[i]);
        i++;
    } 
*/

/* 1. FT_MEMSET 
    size_t  n;
    void    *s;
	void	*s_test;
    int     c;
    char    str[5];
	char	str_test[5];
    size_t  i;

    s = &str[0];
	s_test = &str_test[0];
    n = 3;
    c = 49;
    i = 0;

    printf("n = %d\n", n); 
    printf("c = %d\n", c); 
	ft_memset(NULL, c, n);
    while (i < n)
    {
        printf("%c\n", str[i]);
        i++;
    }
	i = 0;
    printf("Original memset\n");
    memset(NULL, c, n);
    while (i < n)
    {
        printf("%c\n", str_test[i]);
        i++;
    }
*/
    return (0);
}