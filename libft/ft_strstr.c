/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 18:32:57 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/29 20:17:42 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	size_t	flag;
	size_t	len_s2;
	char	*s;

	i = 0;
	flag = 0;
	s = (char *)s1;
	len_s2 = ft_strlen(s2);
	if (len_s2 == 0)
		return (s);
	while (s[i])
	{
		while (s[i + flag] == s2[flag])
		{
			if (flag == len_s2 - 1)
				return (&s[i]);
			flag++;
		}
		flag = 0;
		i++;
	}
	return (NULL);
}
