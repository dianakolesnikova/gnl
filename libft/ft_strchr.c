/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 17:44:33 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/09 17:09:39 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strchr(const char *s, int c)
{
	char	*s_ret;

	s_ret = (char *)s;
	while (*s_ret)
	{
		if (*s_ret == (char)c)
			return (s_ret);
		s_ret++;
	}
	if (*s_ret == (char)c)
		return (s_ret);
	return (NULL);
}
