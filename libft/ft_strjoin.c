/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/30 18:44:12 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:41:30 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strjoin(char const *s1, char const *s2)
{
	char	*str_con;
	size_t	i;

	i = 0;
	if (s1 && s2)
	{
		if (!(str_con = (char *)malloc(sizeof(char) * (ft_strlen((char *)s1) \
		+ ft_strlen((char *)s2) + 1))))
			return (NULL);
		while (s1[i])
		{
			str_con[i] = s1[i];
			i++;
		}
		while (s2[i - ft_strlen((char *)s1)])
		{
			str_con[i] = s2[i - ft_strlen((char *)s1)];
			i++;
		}
		str_con[i] = '\0';
		return (str_con);
	}
	return (NULL);
}
