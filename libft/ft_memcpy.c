/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 14:57:48 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/12 16:58:30 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t			i;
	unsigned char	*dst_uns;
	unsigned char	*src_uns;

	if (src == dst || n == 0)
		return (dst);
	dst_uns = (unsigned char *)dst;
	src_uns = (unsigned char *)src;
	i = 0;
	while (i < n)
	{
		dst_uns[i] = src_uns[i];
		i++;
	}
	return (dst);
}
