/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/30 15:09:42 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:41:04 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strmap(char const *s, char (*f)(char))
{
	char	*mapline;
	size_t	i;

	i = 0;
	if (s && f)
	{
		if (!(mapline = (char *)malloc(sizeof(char) * (ft_strlen((char *)s)
			+ 1))))
			return (NULL);
		while (s[i])
		{
			mapline[i] = f(s[i]);
			i++;
		}
		mapline[i] = '\0';
		return (mapline);
	}
	return (NULL);
}
