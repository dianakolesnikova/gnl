/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/02 17:22:47 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/28 21:22:50 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*s_unsd;
	unsigned char	c_unsd;
	size_t			i;

	c_unsd = (unsigned char)c;
	s_unsd = (unsigned char *)s;
	i = 0;
	while (i < n)
	{
		s_unsd[i] = c_unsd;
		i++;
	}
	return (s);
}
