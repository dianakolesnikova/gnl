/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 16:30:47 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:04:10 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*dst_uns;
	unsigned char	*src_uns;
	unsigned char	c_uns;
	void			*res;

	if (src == dst || n == 0)
		return (NULL);
	dst_uns = (unsigned char *)dst;
	src_uns = (unsigned char *)src;
	c_uns = (unsigned char)c;
	i = 0;
	while (i < n)
	{
		if (src_uns[i] == c_uns)
		{
			dst_uns[i] = c_uns;
			res = &dst_uns[i + 1];
			return (res);
		}
		else
			dst_uns[i] = src_uns[i];
		i++;
	}
	return (NULL);
}
