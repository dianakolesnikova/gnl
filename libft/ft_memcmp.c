/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 22:31:51 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:06:00 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*s1_uns;
	unsigned char	*s2_uns;
	size_t			i;

	i = 0;
	if (n == 0)
		return (0);
	s1_uns = (unsigned char *)s1;
	s2_uns = (unsigned char *)s2;
	while ((i + 1 < n) && s1_uns[i] == s2_uns[i])
		i++;
	return (s1_uns[i] - s2_uns[i]);
}
