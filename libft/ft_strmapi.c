/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/30 15:10:00 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:41:10 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char				*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*mapline;
	unsigned int	i;

	i = 0;
	if (s && f)
	{
		if (!(mapline = (char *)malloc(sizeof(char) * (ft_strlen((char *)s)
			+ 1))))
			return (NULL);
		while (s[i])
		{
			mapline[i] = f(i, s[i]);
			i++;
		}
		mapline[i] = '\0';
		return (mapline);
	}
	return (NULL);
}
