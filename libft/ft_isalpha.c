/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 21:42:50 by gmance-r          #+#    #+#             */
/*   Updated: 2018/12/30 14:37:25 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isalpha(int c)
{
	unsigned char c_uns;

	if (c >= 0 && c <= 255)
	{
		c_uns = (unsigned char)c;
		if ((c_uns >= 'A' && c_uns <= 'Z') ||
			(c_uns >= 'a' && c_uns <= 'z'))
			return (1);
	}
	return (0);
}
