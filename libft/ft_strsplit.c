/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/30 19:32:41 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:41:56 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t		ft_wordcount(char *s_trmd, char c)
{
	size_t			wc;
	size_t			i;

	wc = 0;
	i = 0;
	while (s_trmd[i])
	{
		if ((s_trmd[i] != c) && (s_trmd[i + 1] == c || s_trmd[i + 1] == '\0'))
			wc++;
		i++;
	}
	return (wc);
}

static size_t		ft_wordlen(char *s_trmd, char c)
{
	size_t			i;

	i = 0;
	while (*s_trmd == c)
		s_trmd++;
	while (*s_trmd != c && *s_trmd != '\0')
	{
		s_trmd++;
		i++;
	}
	return (i);
}

static char			*ft_free_all(char ***wrd, size_t i)
{
	size_t			j;

	j = 0;
	while (j < i)
	{
		free((*wrd)[j]);
		(*wrd)[j] = NULL;
		j++;
	}
	free(*wrd);
	*wrd = NULL;
	return (NULL);
}

static char			*ft_wordcopy(char **wrd, char *s_trmd, char c, size_t i)
{
	size_t			j;

	if (!(wrd[i] = (char *)malloc(sizeof(char) * (ft_wordlen(s_trmd, c)\
														+ 1))) && i > 0)
		return (ft_free_all(&wrd, i));
	j = 0;
	while (*s_trmd == c)
		s_trmd++;
	while (*s_trmd != c && *s_trmd != '\0')
	{
		wrd[i][j] = *s_trmd;
		j++;
		s_trmd++;
	}
	wrd[i][j] = '\0';
	return (s_trmd);
}

char				**ft_strsplit(char const *s, char c)
{
	char			*s_trmd;
	char			**wrd;
	size_t			i;
	size_t			wc;

	if (s)
	{
		i = 0;
		s_trmd = (char *)s;
		wc = ft_wordcount(s_trmd, c);
		if (!(wrd = (char **)malloc(sizeof(char *) * (wc + 1))))
			return (NULL);
		while (i < wc)
		{
			if (!(s_trmd = ft_wordcopy(wrd, s_trmd, c, i)))
				return (NULL);
			i++;
		}
		wrd[i] = NULL;
		return (wrd);
	}
	return (NULL);
}
