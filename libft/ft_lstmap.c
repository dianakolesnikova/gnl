/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 17:47:18 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/17 16:44:10 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void			ft_lst_free_all(t_list **begin)
{
	t_list			*tmp;

	while ((*begin)->next)
	{
		tmp = (*begin)->next;
		free(*begin);
		*begin = tmp;
	}
	*begin = NULL;
}

t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list			*new_list;
	t_list			*begin;

	if (lst != NULL & f != NULL)
	{
		if (!(new_list = f(lst)))
			return (NULL);
		begin = new_list;
		while (lst->next)
		{
			if (!(new_list->next = f(lst->next)))
			{
				ft_lst_free_all(&begin);
				return (NULL);
			}
			lst = lst->next;
			new_list = new_list->next;
		}
		return (begin);
	}
	return (NULL);
}
