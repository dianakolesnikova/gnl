/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/28 20:31:41 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/07 19:27:55 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void			*ft_memcpy_rev(void *dst, const void *src, size_t n)
{
	size_t			i;
	unsigned char	*dst_uns;
	unsigned char	*src_uns;

	dst_uns = (unsigned char *)dst;
	src_uns = (unsigned char *)src;
	i = 0;
	while (n != 0)
	{
		dst_uns[n - 1] = src_uns[n - 1];
		n--;
	}
	dst_uns[n] = src_uns[n];
	return (dst);
}

void				*ft_memmove(void *dst, const void *src, size_t n)
{
	size_t			i;

	i = 0;
	if (src == dst || n == 0)
		return (dst);
	else if (src > dst)
		ft_memcpy(dst, src, n);
	else
		ft_memcpy_rev(dst, src, n);
	return (dst);
}
