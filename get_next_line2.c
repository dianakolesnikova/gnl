/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:21:25 by gmance-r          #+#    #+#             */
/*   Updated: 2019/02/11 18:37:00 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

void				ft_lstnew_fd(t_list **tail, size_t fd)
{
	t_list			*new_elem;
	
	if (!*tail)
	{
		if (!(*tail = (t_list *)malloc(sizeof(t_list))))
			return ;
		(*tail)->content = NULL;
		(*tail)->content_size = fd;
		(*tail)->next = NULL;
	}
	else 
	{
		if (!(new_elem = (t_list *)malloc(sizeof(t_list))))
			return ;
		(*tail)->next = new_elem;
		new_elem->content = NULL;
		new_elem->content_size = fd;
		new_elem->next = NULL;
	}
}

static void			ft_getbeforen(char *temp, char **line, int end)
{
	char			*last_piece;
	size_t			i;
	char			*temp_line;

	//printf("temp ft = %s\n", temp);
	//printf("line = %s\n", *line);
	temp_line = NULL;
	i = 0;
	if (end < BUFF_SIZE )
	{
		while (temp[i] != '\n' && temp[i] != '\0') // В том, что получили ищем переход на новую строку ! Ошибка в том, что на последнем куске, где нет перехода на новую строку, он не находит
			i++;
		// i = end;
	}
	else 
	{
		while (temp[i] != '\n')
			i++;
	}
	last_piece = ft_memalloc(i + 1);
	last_piece = ft_memcpy(last_piece, temp, i);
	last_piece[i] = '\0';
	//printf("last_piece = %s\n", last_piece);
	// (temp_line == NULL) ? (temp_line = ft_strdup(last_piece)) : (temp_line = ft_strjoin(*line, last_piece));
	/* if (*line == NULL)
		temp_line = ft_strdup(last_piece);
	else
	{ 
		temp_line = ft_strjoin(*line, last_piece);
		*line = ft_strdup(temp_line);
		free(temp_line);
	} */
	free(last_piece);
}

static void			ft_saveaftern(t_list **tail, char *temp_buf, char **line, int end)
{
	char			*tail_line;
	char			*temp;

	temp = ft_strdup(temp_buf); // Сохранили в temp то, что передали в буфер
	if (!(tail_line = ft_strchr(temp_buf, '\n')))
		tail_line = ft_strchr(temp_buf, '\0'); // вот здесь нужно исправить. Зачем вообще нужна эта строчка? // Нужна, так как без нее ломается 77 строчка (if (tail_line + 1))
	//	tail_line = NULL;
	//printf("tail_line = %s\n", tail_line);
	if ((*tail)->content)
		free((*tail)->content);
	if (tail_line + 1)
	{
		(*tail)->content = ft_strdup(tail_line + 1);
	//	printf("tail->content = %s\n", (*tail)->content);
		ft_getbeforen(temp, line, end);
	}
	else
	{
		free((*tail)->content);
		(*tail)->content = NULL;
	}
	free(temp);
}

static int			ft_copy_line(t_list **tail, char **line, int fd)
{
	char			buf[BUFF_SIZE + 1];
	int				end;

	end = BUFF_SIZE;
	if ((*tail)->content) // Если элемент списка, который мы передали, содержит что-то с прошлого раза
	{
		//free(*line);
		//*line = ft_strdup((*tail)->content);
		if (ft_strchr(((*tail)->content), '\n')) // Если в элементе есть переход на новую строку
		{
			ft_saveaftern(tail, (*tail)->content, line, end); // То мы передаем все в следующую функцию, чтобы сохранить информацию (в какой элемент, что сохранять, куда)
			return (1);
		}
		(*line == NULL) ? (*line = ft_strdup((*tail)->content)) : (*line = ft_strjoin(*line, (*tail)->content));
		free((*tail)->content);
		(*tail)->content = NULL;
	}
	//else
		//*line = NULL;
	ft_bzero(buf, BUFF_SIZE + 1);	
	while ((end = read(fd, buf, BUFF_SIZE)))
	{
		buf[end] = '\0';
	//	printf("buf = %s\n", buf);
	//	printf("end = %d\n", end);
		if ((ft_strchr(buf, '\n')) || end < BUFF_SIZE)
		{
			ft_saveaftern(tail, buf, line, end);
			return (1);
		}
		(*line == NULL) ? (*line = ft_strdup(buf)) : (*line = ft_strjoin(*line, buf));
	}
	//end = read(fd, buf, BUFF_SIZE);
	//printf("end = %d\n", end);
	//printf("line = %d\n", **line);
	//printf("tail->content = %s\n", (*tail)->content);
	if ((*tail)->content == NULL && (!(end = read(fd, buf, BUFF_SIZE))) && (*line == NULL)) // удалить лист
		return (0);
	return (1);
}

int					get_next_line(const int fd, char **line)
{
	static t_list	*tail;
	static t_list	*begin;

	//if (*line != NULL) // Чтобы та линия, которая выводится не содержала остатков предыдущего вывода
	//{
	//	free(*line); // подумать, если там мусор при первом запуске. Можно просто NULL присвоить 
		*line = NULL;
	//}
	if (fd == -1 || !line || BUFF_SIZE < 1)
		return (-1);
	if (!tail) // Если код запускается в первый раз, то создаем связанный список из структур. Начало списка запоминаем в begin
	{
		ft_lstnew_fd(&tail, (size_t)fd);
		begin = tail;
		return (ft_copy_line(&tail, line, fd)); 
	}
	else // Если лист уже существует то, возвращаемся на начало. И идем до тех пор, пока next не будет равен NULL (то есть пока не наталкиваемся на последний элемент списка)
	{
		//printf("tail->content = %s\n", (tail)->content);
		tail = begin;
		//	printf("fd = %d\n", fd);
		while (!(tail->content_size == (size_t)fd || tail->next == NULL)) // || tail->next == NULL Перебираем элементы списка, чтобы найти с fd нужного файла или конец списка
			tail = tail->next;
		if (tail->content_size == (size_t)fd)
			return (ft_copy_line(&tail, line, fd)); // Обращаемся к элементу списка соответствующего fd
		else
		{
			ft_lstnew_fd(&tail, (size_t)fd); // Создаем новый элемент списка
			tail = tail->next;
			return (ft_copy_line(&tail, line, fd));  
		}
	}
}
