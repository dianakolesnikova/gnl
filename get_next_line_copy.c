/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:21:25 by gmance-r          #+#    #+#             */
/*   Updated: 2019/01/30 20:50:27 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

void		ft_lstnew_fd(t_list **tail, size_t fd)
{
	if (!(*tail = (t_list *)malloc(sizeof(t_list))))
		return ;
	(*tail)->content = NULL;
	(*tail)->content_size = fd;
	(*tail)->next = NULL;
}


int					get_next_line(const int fd, char **line)
{
	char			buf[BUFF_SIZE + 1];
	char			*tmp_line;
	char			*last_piece;
	char			*tail_line;
	int				end;
	size_t			i;
	static t_list	*tail;
	static t_list	*begin;
	static t_list	*tmp_tail;

	
	i = 0;
	tmp_line = NULL;
	if (fd == -1 || !line || BUFF_SIZE < 1)
		return (-1);
	if (!tail)
	{
		ft_lstnew_fd(&tail, (size_t)fd); 
		begin = tail;
	}	
	else 
	{
		tail = begin;
		while (!((tail->content_size == (size_t)fd) || (tail->next == NULL)))
			tail = tail->next;
		if (tail->content_size == (size_t)fd) 
		{
			tmp_line = ft_strdup(tail->content);
		//	printf("tmp_line 0 = %s\n", tmp_line);
			if (((tail_line = ft_strchr(tmp_line, '\n')))) /* || (tail_line = ft_strchr(tmp_line, '\0')))) */
			{
		//		printf("tail_line 0 = %s\n", tail_line);
				while (tmp_line[i] != '\n') /* && tmp_line[i] != '\0') */
					i++;
				last_piece = ft_memalloc(i + 1);
				last_piece = ft_memcpy(last_piece, tmp_line, i);
				last_piece[i] = '\0';
				free(tmp_line);
				tmp_line = ft_strdup(last_piece);
				*line = tmp_line;
				free(tail->content);
				tail->content =	ft_strdup(tail_line + 1);
				return (1);
			}
			else
			{
				tmp_line = ft_strdup(tail->content);
			//	printf("kuku1\n");
				free(tail->content);
				//	tail->content = NULL;
			}
		}
		else
		{
			ft_lstnew_fd(&tmp_tail, (size_t)fd); 
			tail->next = tmp_tail;
			tail = tail->next;
		//	free(tmp_tail);
		}	
	}	
	i = 0;
	while ((end = read(fd, buf, BUFF_SIZE)))
	{
		buf[end] = '\0';
	//	printf("buf = %s\n", buf);
		if ((tail_line = ft_strchr(buf, '\n')) || end < BUFF_SIZE)
		{
			if (end < BUFF_SIZE && tail_line == NULL)
				tail_line = ft_strchr(buf, '\0');
			tail->content =	ft_strdup(tail_line + 1);
		//	printf("tail_line = %s\n", tail_line);
		//	printf("tail->content = %s\n", tail->content);
			while (buf[i] != '\n' && buf[i] != '\0')
				i++;
			last_piece = ft_memalloc(i + 1);
			last_piece = ft_memcpy(last_piece, buf, i);
			last_piece[i] = '\0';
		//	printf("last_piece = %s\n", last_piece);
			(tmp_line == NULL) ? (tmp_line = ft_strdup(last_piece)) : (tmp_line = ft_strjoin(tmp_line, last_piece));
			free(last_piece);
			*line = tmp_line;
			return (1);
		}
		(tmp_line == NULL) ? (tmp_line = ft_strdup(buf)) : (tmp_line = ft_strjoin(tmp_line, buf));
		//printf("tmp_line = %s\n", tmp_line);
	}
	return (0);
}
