/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_main.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:27:16 by gmance-r          #+#    #+#             */
/*   Updated: 2019/02/15 19:22:46 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

int			main(int argc, char **argv)
{
	int		fd;
	 int		res;

	int       fd1;
    int       fd2;
    int       fd3;
	char	*line;
	char	*line2;
	
	res = 0;
	line = NULL;
	line2 = NULL;

	fd = 0;
	fd1 = 0;
	fd2 = 0;

	fd3 = -1;
	if (argc < 2)
		write(2, "File name missing.\n", 19);
	// else
	// {
	// 	fd = open(argv[1], O_RDONLY);
	// 	while((res = get_next_line((const int)fd, &line)) == 1)
	// 	{
	// 		printf("res = %d\n", res);
	// 		printf("main 1 = %s\n", line);
	// 	}
	// 	printf("res res = %d\n", res);
	// 	printf("main 1 line = %s\n", line);
	// 	printf("\n");
	// 	/*if (close(fd) == -1) 
	// 	{
	// 		write(2, "close() failed\n", 15);
	// 		return (-1);
	// 	} 
	// 	*/
	// 	fd = open(argv[2], O_RDONLY);
	// 	while(get_next_line((const int)fd, &line2) == 1)
	// 		printf("main 2 = %s\n", line2);
	// 	printf("\n");
	// }
	// 	fd = open(argv[3], O_RDONLY);
	// 	while(get_next_line((const int)fd, &line) == 1)
	// 		printf("main 3 = %s\n", line);
	// 	printf("\n");

		fd1 = open(argv[1], O_RDONLY);
		fd2 = open(argv[2], O_RDONLY);

 // Первый файл, 1 строка
        if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

 		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		 if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		 if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);
    
		printf("\n");
// Вторй файл, 1 строка
        if(get_next_line((const int)fd2, &line2))
            printf("main 2 = %s\n", line2);


        if(get_next_line((const int)fd2, &line2))
            printf("main 2 = %s\n", line2);

		 if(get_next_line((const int)fd2, &line2))
            printf("main 2 = %s\n", line2);

		if(get_next_line((const int)fd2, &line2))
            printf("main 2 = %s\n", line2);
		printf("\n");
	//	printf("fd2 = %d\n\n", fd2);

// Первый файл, 5 строка
	    if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);

		if(get_next_line((const int)fd1, &line))
            printf("main 1 = %s\n", line);	

	printf("\n");
	//	printf("fd1 = %d\n\n", fd1);

	//printf("fd3 = %d\n", get_next_line(fd3, &line));

// Вторй файл, 5 строка
		if((res = get_next_line((const int)fd2, &line2)))
		{
           // printf("fd2 = %d\n", fd2);
			printf("main 2 = %s\n", line2);
		 	printf("res = %d\n", res);
		 	printf("diff = %d\n\n", strcmp(line2, "The city is a center for cultural events since there are many students. "));
		}


        // fd3 = open(argv[3], O_RDONLY);
        // if(get_next_line((const int)fd3, &line))
        //     printf("main 3 = %s\n", line);

       

        // if(get_next_line((const int)fd2, &line))
        //     printf("main 2 = %s\n\n", line);

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);

        // if(get_next_line((const int)fd1, &line))
        //     printf("main 1 = %s\n", line);

        // if(get_next_line((const int)fd2, &line))
        //     printf("main 2 = %s\n\n", line);

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);

        // if(get_next_line((const int)fd1, &line))
        //     printf("main 1 = %s\n", line);

        // if(get_next_line((const int)fd2, &line))
        //     printf("main 2 = %s\n\n", line);

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);

        // if(get_next_line((const int)fd1, &line))
        //     printf("main 1 = %s\n", line);

        // if((res = get_next_line((const int)fd2, &line)))
		// {
        //     printf("main 2 = %s\n", line);
		// 	printf("res = %d\n", res);
		// 	printf("diff = %d\n\n", strcmp(line, "The city is a center for cultural events since there are many students. "));
		// }

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);

        // if(get_next_line((const int)fd1, &line))
        //     printf("main 1 = %s\n", line);

        // if(get_next_line((const int)fd2, &line))
        //     printf("main 2 = %s\n\n", line);

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);

        // if(get_next_line((const int)fd1, &line))
        //     printf("main 1 = %s\n", line);

        // if(get_next_line((const int)fd2, &line))
        //     printf("main 2 = %s\n\n", line);

        // // if(get_next_line((const int)fd3, &line))
        // //     printf("main 3 = %s\n", line);
	
	
	return (0);
}
