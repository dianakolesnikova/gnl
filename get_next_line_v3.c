/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_v3.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmance-r <gmance-r@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:21:25 by gmance-r          #+#    #+#             */
/*   Updated: 2019/02/03 19:00:25 by gmance-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

void				ft_lstnew_fd(t_list **tail, size_t fd)
{
	if (!(*tail = (t_list *)malloc(sizeof(t_list))))
		return ;
	(*tail)->content = NULL;
	(*tail)->content_size = fd;
	(*tail)->next = NULL;
}

static int			ft_saveaftern(t_list *tail, char *buf)
{
	char			*tail_line;

	if (!(tail_line = ft_strchr(buf, '\n')))
		tail_line = ft_strchr(buf, '\0');
	if (tail->content)
		free(tail->content);
	if (tail_line + 1)
		tail->content = ft_strdup(tail_line + 1);
	else
		tail->content = NULL;
	return (1);
}

static void			ft_getbeforen(char *buf, char **line)
{
	char			*last_piece;
	size_t			i;

	i = 0;
	while (buf[i] != '\n')
		i++;
	last_piece = ft_memalloc(i + 1);
	last_piece = ft_memcpy(last_piece, buf, i);
	last_piece[i] = '\0';
	*line = ft_strdup(last_piece);
	free(last_piece);
}

static int			ft_copy_line(t_list *tail, char **line, int fd)
{
	char			buf[BUFF_SIZE + 1];
	int				end;

	*line = ft_strdup(tail->content);
	if (ft_strchr(*line, '\n'))
	{
		ft_getbeforen(*line, line);
		ft_saveaftern(&tail, *line);
	}
	else
	{
		while (end = read(fd, buf, BUFF_SIZE))
		{
			buf[end] = '\0';
			if ((ft_strchr(buf, '\n')) || end < BUFF_SIZE)
			{
				ft_getbeforen(buf, line);
				ft_saveaftern(&tail, buf);
			}
			*line = ft_strjoin(*line, buf);
		}
	}
	return (0);
}

int					get_next_line(const int fd, char **line)
{
	t_list			*tail;
	static t_list	*begin;

	if (fd == -1 || !line || BUFF_SIZE < 1)
		return (-1);
	if (!tail)
	{
		ft_lstnew_fd(&tail, (size_t)fd);
		begin = tail;
	}
	else
	{
		tail = begin;
		while (tail->next != NULL)
		{
			if (tail->content_size == (size_t)fd)
				return (ft_copy_line(&tail, line, fd));
			tail = tail->next;
		}
		ft_lstnew_fd(&tail, (size_t)fd);
	}
	return (ft_copy_line(&tail, line, fd));
}
